
#include "MNG_InternalTyp.hpp"

unsigned long int MNG_CpyOutToMsg(MNG_RawDatagram * pToServer, unsigned long int counter, COM_BUR_OUTPUTS_32BIT_FWRD *pSimOutputs ) {

    
        unsigned long int DWordBuffer;
	
        pSimOutputs->NumSignals = 49;
        (pToServer->Header).ByteCount    = H_TO_NUDINT(200);
        (pToServer->Header).DatagramType = H_TO_NUDINT(MNG_Raw32BitImage);
        (pToServer->Header).SentTime = H_TO_NUDINT(0);  // Todo
        (pToServer->Header).Counter = H_TO_NUDINT(counter);
        pToServer->Payload[0] = H_TO_NUDINT(pSimOutputs->NumSignals);

    
    /* Transfer_1_Pallet_1_X : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_1_X), 4);
    pToServer->Payload[1] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_1_Y : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_1_Y), 4);
    pToServer->Payload[2] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_1_Z : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_1_Z), 4);
    pToServer->Payload[3] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_2_X : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_2_X), 4);
    pToServer->Payload[4] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_2_Y : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_2_Y), 4);
    pToServer->Payload[5] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_2_Z : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_2_Z), 4);
    pToServer->Payload[6] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_3_X : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_3_X), 4);
    pToServer->Payload[7] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_3_Y : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_3_Y), 4);
    pToServer->Payload[8] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_3_Z : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_3_Z), 4);
    pToServer->Payload[9] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_4_X : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_4_X), 4);
    pToServer->Payload[10] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_4_Y : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_4_Y), 4);
    pToServer->Payload[11] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_4_Z : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_4_Z), 4);
    pToServer->Payload[12] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_5_X : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_5_X), 4);
    pToServer->Payload[13] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_5_Y : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_5_Y), 4);
    pToServer->Payload[14] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_5_Z : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_5_Z), 4);
    pToServer->Payload[15] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_6_X : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_6_X), 4);
    pToServer->Payload[16] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_6_Y : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_6_Y), 4);
    pToServer->Payload[17] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_6_Z : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_6_Z), 4);
    pToServer->Payload[18] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_7_X : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_7_X), 4);
    pToServer->Payload[19] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_7_Y : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_7_Y), 4);
    pToServer->Payload[20] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_7_Z : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_7_Z), 4);
    pToServer->Payload[21] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_8_X : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_8_X), 4);
    pToServer->Payload[22] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_8_Y : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_8_Y), 4);
    pToServer->Payload[23] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_8_Z : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_8_Z), 4);
    pToServer->Payload[24] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_9_X : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_9_X), 4);
    pToServer->Payload[25] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_9_Y : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_9_Y), 4);
    pToServer->Payload[26] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_9_Z : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_9_Z), 4);
    pToServer->Payload[27] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_10_X : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_10_X), 4);
    pToServer->Payload[28] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_10_Y : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_10_Y), 4);
    pToServer->Payload[29] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_10_Z : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_10_Z), 4);
    pToServer->Payload[30] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_11_X : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_11_X), 4);
    pToServer->Payload[31] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_11_Y : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_11_Y), 4);
    pToServer->Payload[32] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_11_Z : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_11_Z), 4);
    pToServer->Payload[33] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_12_X : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_12_X), 4);
    pToServer->Payload[34] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_12_Y : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_12_Y), 4);
    pToServer->Payload[35] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_12_Z : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_12_Z), 4);
    pToServer->Payload[36] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_13_X : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_13_X), 4);
    pToServer->Payload[37] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_13_Y : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_13_Y), 4);
    pToServer->Payload[38] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_13_Z : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_13_Z), 4);
    pToServer->Payload[39] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_14_X : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_14_X), 4);
    pToServer->Payload[40] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_14_Y : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_14_Y), 4);
    pToServer->Payload[41] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_14_Z : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_14_Z), 4);
    pToServer->Payload[42] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_15_X : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_15_X), 4);
    pToServer->Payload[43] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_15_Y : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_15_Y), 4);
    pToServer->Payload[44] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_15_Z : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_15_Z), 4);
    pToServer->Payload[45] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_16_X : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_16_X), 4);
    pToServer->Payload[46] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_16_Y : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_16_Y), 4);
    pToServer->Payload[47] = H_TO_NUDINT(DWordBuffer);


    /* Transfer_1_Pallet_16_Z : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Transfer_1_Pallet_16_Z), 4);
    pToServer->Payload[48] = H_TO_NUDINT(DWordBuffer);


    /* BuR_IP_Transfer_1_ST_Ready : BOOL */
    if(pSimOutputs->BuR_IP_Transfer_1_ST_Ready == 1) {
        pToServer->Payload[49] = 0xFFFFFFFF;   /*BOOL */
    } else { 
        pToServer->Payload[49] = 0;
    }


	return 0;
}
