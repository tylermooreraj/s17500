/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1621889598_4_
#define _BUR_1621889598_4_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
#else
#endif


/* Variables */
_GLOBAL plcbit COM_BUR_SimActive;
_GLOBAL plctime COM_BUR_SimSendCycleTime;
_GLOBAL unsigned short COM_BUR_SimServerPort;
_GLOBAL plcstring COM_BUR_SimServerAddress[16];
_GLOBAL struct COM_BUR_OUTPUTS_32BIT COM_BUR_SimOutputs;
_GLOBAL struct COM_BUR_INPUTS_32BIT COM_BUR_SimInputs;





__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/MNG_Package.var\\\" scope \\\"global\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1621889598_4_ */

