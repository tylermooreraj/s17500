/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1621889598_3_
#define _BUR_1621889598_3_

#include <bur/plctypes.h>

/* Datatypes and datatypes of function blocks */
typedef struct COM_BUR_INPUTS_32BIT
{	signed short NumSignals;
	float Transfer_1_Pallet_16_X_Actual;
	plcbit IP_TO_BuR_Transfer_Elevator_PE;
	plcbit IP_BuR_Transfer_1_MULTI_MODE;
	plcbit IP_TO_BuR_Trans_1_Elev_Complete;
} COM_BUR_INPUTS_32BIT;

typedef struct COM_BUR_OUTPUTS_32BIT
{	signed short NumSignals;
	float Transfer_1_Pallet_1_X;
	float Transfer_1_Pallet_1_Y;
	float Transfer_1_Pallet_1_Z;
	float Transfer_1_Pallet_2_X;
	float Transfer_1_Pallet_2_Y;
	float Transfer_1_Pallet_2_Z;
	float Transfer_1_Pallet_3_X;
	float Transfer_1_Pallet_3_Y;
	float Transfer_1_Pallet_3_Z;
	float Transfer_1_Pallet_4_X;
	float Transfer_1_Pallet_4_Y;
	float Transfer_1_Pallet_4_Z;
	float Transfer_1_Pallet_5_X;
	float Transfer_1_Pallet_5_Y;
	float Transfer_1_Pallet_5_Z;
	float Transfer_1_Pallet_6_X;
	float Transfer_1_Pallet_6_Y;
	float Transfer_1_Pallet_6_Z;
	float Transfer_1_Pallet_7_X;
	float Transfer_1_Pallet_7_Y;
	float Transfer_1_Pallet_7_Z;
	float Transfer_1_Pallet_8_X;
	float Transfer_1_Pallet_8_Y;
	float Transfer_1_Pallet_8_Z;
	float Transfer_1_Pallet_9_X;
	float Transfer_1_Pallet_9_Y;
	float Transfer_1_Pallet_9_Z;
	float Transfer_1_Pallet_10_X;
	float Transfer_1_Pallet_10_Y;
	float Transfer_1_Pallet_10_Z;
	float Transfer_1_Pallet_11_X;
	float Transfer_1_Pallet_11_Y;
	float Transfer_1_Pallet_11_Z;
	float Transfer_1_Pallet_12_X;
	float Transfer_1_Pallet_12_Y;
	float Transfer_1_Pallet_12_Z;
	float Transfer_1_Pallet_13_X;
	float Transfer_1_Pallet_13_Y;
	float Transfer_1_Pallet_13_Z;
	float Transfer_1_Pallet_14_X;
	float Transfer_1_Pallet_14_Y;
	float Transfer_1_Pallet_14_Z;
	float Transfer_1_Pallet_15_X;
	float Transfer_1_Pallet_15_Y;
	float Transfer_1_Pallet_15_Z;
	float Transfer_1_Pallet_16_X;
	float Transfer_1_Pallet_16_Y;
	float Transfer_1_Pallet_16_Z;
	plcbit BuR_IP_Transfer_1_ST_Ready;
} COM_BUR_OUTPUTS_32BIT;






__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/MNG_Package.typ\\\" scope \\\"global\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1621889598_3_ */

