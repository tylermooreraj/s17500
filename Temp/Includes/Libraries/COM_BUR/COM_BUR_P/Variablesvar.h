/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1621889598_7_
#define _BUR_1621889598_7_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
#else
#endif


/* Variables */
_BUR_LOCAL signed short connectionState;





__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/Libraries/COM_BUR/COM_BUR_P/Variables.var\\\" scope \\\"local\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1621889598_7_ */

