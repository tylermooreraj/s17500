/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1621889598_2_
#define _BUR_1621889598_2_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
#else
#endif


/* Variables */
_GLOBAL signed char DataOut[490];
_GLOBAL signed char DataIn[490];
_GLOBAL unsigned long gSuperTrak;





__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/Global.var\\\" scope \\\"global\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1621889598_2_ */

