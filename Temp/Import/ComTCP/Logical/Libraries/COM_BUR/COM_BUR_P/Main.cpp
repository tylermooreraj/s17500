
#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
        #include <AsDefault.h>
#endif

#include "../COM_BUR_L/MNG_Global.hpp"
#include "../COM_BUR_L/MNG_InternalTyp.hpp"
#include "../COM_BUR_L/MNG_ComHdlr.hpp"

unsigned long bur_heap_size = 0xFFFF;


MNG_ComHdlr *pMNG_ComHdlr;


void _INIT ProgramInit(void)
{
    pMNG_ComHdlr = new MNG_ComHdlr(COM_BUR_SimServerAddress, COM_BUR_SimServerPort, COM_BUR_SimSendCycleTime);
}

void _CYCLIC ProgramCyclic(void)
{
    connectionState = pMNG_ComHdlr->RunCommunication((void*)&COM_BUR_SimInputs,(void*)&COM_BUR_SimOutputs, &COM_BUR_SimActive);
}

void _EXIT ProgramExit(void)
{
    delete pMNG_ComHdlr;
}
