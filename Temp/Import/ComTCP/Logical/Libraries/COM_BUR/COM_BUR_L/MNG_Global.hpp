
#ifndef  _MNGGLOBAL_HPP_
#define _MNGGLOBAL_HPP_

#include <mng_globalTYP.h>


    typedef struct {

		signed short NumSignals;

        /* REAL Transfer_1_Pallet_16_X_Actual */
        REAL Transfer_1_Pallet_16_X_Actual;
        /* BOOL IP_TO_BuR_Transfer_Elevator_PE */
        BOOL IP_TO_BuR_Transfer_Elevator_PE;
        /* BOOL IP_BuR_Transfer_1_MULTI_MODE */
        BOOL IP_BuR_Transfer_1_MULTI_MODE;
        /* BOOL IP_TO_BuR_Trans_1_Elev_Complete */
        BOOL IP_TO_BuR_Trans_1_Elev_Complete;

        } COM_BUR_INPUTS_32BIT_FWRD;


    typedef struct {

		signed short NumSignals;

        /* REAL Transfer_1_Pallet_1_X */
        REAL Transfer_1_Pallet_1_X;
        /* REAL Transfer_1_Pallet_1_Y */
        REAL Transfer_1_Pallet_1_Y;
        /* REAL Transfer_1_Pallet_1_Z */
        REAL Transfer_1_Pallet_1_Z;
        /* REAL Transfer_1_Pallet_2_X */
        REAL Transfer_1_Pallet_2_X;
        /* REAL Transfer_1_Pallet_2_Y */
        REAL Transfer_1_Pallet_2_Y;
        /* REAL Transfer_1_Pallet_2_Z */
        REAL Transfer_1_Pallet_2_Z;
        /* REAL Transfer_1_Pallet_3_X */
        REAL Transfer_1_Pallet_3_X;
        /* REAL Transfer_1_Pallet_3_Y */
        REAL Transfer_1_Pallet_3_Y;
        /* REAL Transfer_1_Pallet_3_Z */
        REAL Transfer_1_Pallet_3_Z;
        /* REAL Transfer_1_Pallet_4_X */
        REAL Transfer_1_Pallet_4_X;
        /* REAL Transfer_1_Pallet_4_Y */
        REAL Transfer_1_Pallet_4_Y;
        /* REAL Transfer_1_Pallet_4_Z */
        REAL Transfer_1_Pallet_4_Z;
        /* REAL Transfer_1_Pallet_5_X */
        REAL Transfer_1_Pallet_5_X;
        /* REAL Transfer_1_Pallet_5_Y */
        REAL Transfer_1_Pallet_5_Y;
        /* REAL Transfer_1_Pallet_5_Z */
        REAL Transfer_1_Pallet_5_Z;
        /* REAL Transfer_1_Pallet_6_X */
        REAL Transfer_1_Pallet_6_X;
        /* REAL Transfer_1_Pallet_6_Y */
        REAL Transfer_1_Pallet_6_Y;
        /* REAL Transfer_1_Pallet_6_Z */
        REAL Transfer_1_Pallet_6_Z;
        /* REAL Transfer_1_Pallet_7_X */
        REAL Transfer_1_Pallet_7_X;
        /* REAL Transfer_1_Pallet_7_Y */
        REAL Transfer_1_Pallet_7_Y;
        /* REAL Transfer_1_Pallet_7_Z */
        REAL Transfer_1_Pallet_7_Z;
        /* REAL Transfer_1_Pallet_8_X */
        REAL Transfer_1_Pallet_8_X;
        /* REAL Transfer_1_Pallet_8_Y */
        REAL Transfer_1_Pallet_8_Y;
        /* REAL Transfer_1_Pallet_8_Z */
        REAL Transfer_1_Pallet_8_Z;
        /* REAL Transfer_1_Pallet_9_X */
        REAL Transfer_1_Pallet_9_X;
        /* REAL Transfer_1_Pallet_9_Y */
        REAL Transfer_1_Pallet_9_Y;
        /* REAL Transfer_1_Pallet_9_Z */
        REAL Transfer_1_Pallet_9_Z;
        /* REAL Transfer_1_Pallet_10_X */
        REAL Transfer_1_Pallet_10_X;
        /* REAL Transfer_1_Pallet_10_Y */
        REAL Transfer_1_Pallet_10_Y;
        /* REAL Transfer_1_Pallet_10_Z */
        REAL Transfer_1_Pallet_10_Z;
        /* REAL Transfer_1_Pallet_11_X */
        REAL Transfer_1_Pallet_11_X;
        /* REAL Transfer_1_Pallet_11_Y */
        REAL Transfer_1_Pallet_11_Y;
        /* REAL Transfer_1_Pallet_11_Z */
        REAL Transfer_1_Pallet_11_Z;
        /* REAL Transfer_1_Pallet_12_X */
        REAL Transfer_1_Pallet_12_X;
        /* REAL Transfer_1_Pallet_12_Y */
        REAL Transfer_1_Pallet_12_Y;
        /* REAL Transfer_1_Pallet_12_Z */
        REAL Transfer_1_Pallet_12_Z;
        /* REAL Transfer_1_Pallet_13_X */
        REAL Transfer_1_Pallet_13_X;
        /* REAL Transfer_1_Pallet_13_Y */
        REAL Transfer_1_Pallet_13_Y;
        /* REAL Transfer_1_Pallet_13_Z */
        REAL Transfer_1_Pallet_13_Z;
        /* REAL Transfer_1_Pallet_14_X */
        REAL Transfer_1_Pallet_14_X;
        /* REAL Transfer_1_Pallet_14_Y */
        REAL Transfer_1_Pallet_14_Y;
        /* REAL Transfer_1_Pallet_14_Z */
        REAL Transfer_1_Pallet_14_Z;
        /* REAL Transfer_1_Pallet_15_X */
        REAL Transfer_1_Pallet_15_X;
        /* REAL Transfer_1_Pallet_15_Y */
        REAL Transfer_1_Pallet_15_Y;
        /* REAL Transfer_1_Pallet_15_Z */
        REAL Transfer_1_Pallet_15_Z;
        /* REAL Transfer_1_Pallet_16_X */
        REAL Transfer_1_Pallet_16_X;
        /* REAL Transfer_1_Pallet_16_Y */
        REAL Transfer_1_Pallet_16_Y;
        /* REAL Transfer_1_Pallet_16_Z */
        REAL Transfer_1_Pallet_16_Z;
        /* BOOL BuR_IP_Transfer_1_ST_Ready */
        BOOL BuR_IP_Transfer_1_ST_Ready;

        } COM_BUR_OUTPUTS_32BIT_FWRD;



#endif
