
#include "MNG_InternalTyp.hpp"

unsigned long int MNG_CpyMsgToInp(MNG_RawDatagram * pFromServer, COM_BUR_INPUTS_32BIT_FWRD *pSimInputs) {

    MNG_DatagramType datagramType;
	unsigned long int DWordBuffer;
        unsigned long int byteCount;
	unsigned long int numSignalsInMessage;


	byteCount = N_TO_HUDINT((pFromServer->Header).ByteCount);
	datagramType = static_cast<MNG_DatagramType>(N_TO_HUDINT((pFromServer->Header).DatagramType));
	if (datagramType != MNG_Raw32BitImage) {
		return 0;
	}

    pSimInputs->NumSignals = 4;

    numSignalsInMessage = N_TO_HUDINT(pFromServer->Payload[0]);
        if ((static_cast<long int>(numSignalsInMessage) != pSimInputs->NumSignals) ||
	(byteCount != 4 + numSignalsInMessage * 4)) {
		return 0;
	}

    /* FLOAT32 Transfer_1_Pallet_16_X_Actual */
    DWordBuffer = N_TO_HUDINT(pFromServer->Payload[1]);
    brsmemcpy((unsigned long int)&(pSimInputs->Transfer_1_Pallet_16_X_Actual), (unsigned long int)&(DWordBuffer), 4);

    /* BOOL IP_TO_BuR_Transfer_Elevator_PE */
    pSimInputs->IP_TO_BuR_Transfer_Elevator_PE = (pFromServer->Payload[2] != 0);

    /* BOOL IP_BuR_Transfer_1_MULTI_MODE */
    pSimInputs->IP_BuR_Transfer_1_MULTI_MODE = (pFromServer->Payload[3] != 0);

    /* BOOL IP_TO_BuR_Trans_1_Elev_Complete */
    pSimInputs->IP_TO_BuR_Trans_1_Elev_Complete = (pFromServer->Payload[4] != 0);


return N_TO_HUDINT((pFromServer->Header).Counter); // return counter
}
